/*
BT1:
- Lấy giá trị ngày tháng năm
- Xét điều kiện năm nhuận: chia hết cho 4
- Xét điều kiện trong tháng mấy (để tính số ngày trong các tháng)
- Tính toán cộng thêm hoặc trừ đi
*/

function bt1NgayTruoc() {
    let ngay = document.getElementById("ngay").value * 1
    let thang = document.getElementById("thang").value * 1
    let nam = document.getElementById("nam").value * 1
    let resultNgay, resultThang, resultNam

    if (ngay == 1 && (thang == 5 || thang == 7 || thang == 10 || thang == 12)) {
        resultNgay = 30
        resultThang = thang - 1
        resultNam = nam
    } else if (ngay == 1 && (thang == 2 || thang == 4 || thang == 6 || thang == 8 || thang == 9 || thang == 11)) {
        resultNgay = 31
        resultThang = thang - 1
        resultNam = nam
    } else if (ngay == 1 && thang == 3) {
        if (nam % 4 != 0) {
            resultNgay = 28
            resultThang = 2
            resultNam = nam
        } else if (nam % 4 == 0) {
            resultNgay = 29
            resultThang = 2
            resultNam = nam
        }
    } else if (ngay == 1 && thang == 1) {
        resultNgay = 31
        resultThang = 12
        resultNam = nam - 1
    } else {
        resultNgay = ngay - 1
        resultThang = thang
        resultNam = nam
    }
    document.getElementById("resultNgayTruoc").innerHTML = `Ngày trước là: ${resultNgay}/${resultThang}/${resultNam}`
}

function bt1NgaySau() {
    let ngay = document.getElementById("ngay").value * 1
    let thang = document.getElementById("thang").value * 1
    let nam = document.getElementById("nam").value * 1
    let resultNgay, resultThang, resultNam

    if (ngay == 31 && (thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10)) {
        resultNgay = 1
        resultThang = thang + 1
        resultNam = nam
    } else if (ngay == 30 && (thang == 4 || thang == 6 || thang == 9 || thang == 11)) {
        resultNgay = 1
        resultThang = thang + 1
        resultNam = nam
    } else if ((ngay == 29 && thang == 2 && nam % 4 != 0) || (ngay == 28 && thang == 2 && nam % 4 == 0)) {
        resultNgay = 1
        resultThang = 3
        resultNam = nam
    } else if (ngay == 31 && thang == 12) {
        resultNgay = 1
        resultThang = 1
        resultNam = nam + 1
    } else {
        resultNgay = ngay + 1
        resultThang = thang
        resultNam = nam
    }
    document.getElementById("resultNgaySau").innerHTML = `Ngày sau là: ${resultNgay}/${resultThang}/${resultNam}`
}

/*
BT2:
- Lấy giá trị nhâp vào
- Đối với các tháng 1,3,5,7,8,10,12 sẽ có 31 ngày
- Đối với các tháng 4,6,9,11 sẽ có 30 ngày
- Đối với tháng 2, nếu là năm nhuận sẽ có 29 ngày. Ngược lại là 28 ngày.
*/

function bt2() {
    let thang = document.getElementById("thangbt2").value * 1
    let nam = document.getElementById("nambt2").value * 1

    if (thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12) {
        document.getElementById("resultBt2").innerHTML = `Tổng ngày là 31`
    } else if (thang == 4 || thang == 6 || thang == 9 || thang == 11) {
        document.getElementById("resultBt2").innerHTML = `Tổng ngày là 30`
    } else if (thang == 2) {
        if (nam % 4 != 0) {
            document.getElementById("resultBt2").innerHTML = `Tổng ngày là 28`
        } else if (nam % 4 == 0) {
            document.getElementById("resultBt2").innerHTML = `Tổng ngày là 29`
        }
    }
}

/*
BT3:
*/